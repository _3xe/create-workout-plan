(function myApp() {

  const btn = document.querySelector('.btn');
  let table = document.getElementById('workout-plan');
  let number = 0;

  const generatePlanStore = {
    plan() {
        table.innerHTML+=`
        <tr>
          <td>${number+=1}</td>
          <td>${this.exercise}</td>
          <td>${this.repetitions}</td>
          <td>${this.series}</td>
          <td>${this.kilos}</td>
        </tr>`;
    }
  }

  function createWorkout(exercise, repetitions, series, kilos) {
    let newWorkout = Object.create(generatePlanStore);
    newWorkout.exercise = exercise;
    newWorkout.repetitions = repetitions;
    newWorkout.series = series;
    newWorkout.kilos = kilos;
    return newWorkout;
  }

  function generate() {
    number = 0;
    table.innerHTML='';
    const experience = document.getElementById('experience');
    const lvl = experience.value;

    const exercises = [['Bench press', 15], ['Lat pulldown', 15], ['Squats', 15], ['Leg curl', 15], ['Dumbbell shoulder press', 5], ['Triceps press down', 5], ['Deadlifts (Ramped)', 15], ['Seated Overhead Press', 5], ['Dips', 0], ['Barbell Shrugs', 10]];
  
    if(lvl == 1) {
      for(let i = 0; i < exercises.length; i++) {
        let beginner = createWorkout(exercises[i][0], '6-12', 3, exercises[i][1]);
        beginner.plan();
      }
    } else if (lvl == 2) {
      for(let i = 0; i < exercises.length; i++) {
        let intermediate = createWorkout(exercises[i][0], '8-12', 4, exercises[i][1] + 10);
        intermediate.plan();
      }
    } else {
      for(let i = 0; i < exercises.length; i++) {
        let advanced = createWorkout(exercises[i][0], '10-12', 5, exercises[i][1] + 20);
        advanced.plan();
      }
    }
  }

  const check = function () {

    const height = document.getElementById('height').value;
    const weight = document.getElementById('weight').value;
    const age = document.getElementById('age').value;

    const isHeight = height < 100 || height > 200 ? false : true;
    const isWeight = weight < 30 || weight > 150 ? false : true;
    const isAge = age < 14 || age > 100 ? false : true;

    const info = document.querySelector('.info-result');

    if(isHeight && isWeight && isAge) {
      info.innerHTML="";
      return true;
    } else if (height === '' && weight === '' && age === '') {
      return false;
    } else {
      info.style.color="red";
      info.innerHTML="You are not the target of this generator! <br> Consult a real trainer";
      return false;
    }
  }

  btn.addEventListener('click', () => {
    if(check()) {
      generate();
    }
  })
})
();
